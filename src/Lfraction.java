import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {

      Lfraction uusfra = new Lfraction(-2, 4);
      System.out.println(uusfra.getNumerator());
      System.out.println(uusfra.getDenominator());
      System.out.println(uusfra.valueOf("1/2"));
      // TODO!!! Your debugging tests here
   }

   private long denom;
   private long numerat;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {

      if (b==0){
         throw new RuntimeException("Denominator can't be 0");
      }else {

         this.numerat = a;
         this.denom = b;

         long comm;
         long larger;
         long smaller;

         if(this.denom==this.numerat)
         {
            comm = this.denom;
         } else {

            if (this.denom > this.numerat) {
               larger = this.denom;
               smaller = this.numerat;
            } else {
               larger = this.numerat;
               smaller = this.denom;
            }

            long intermed;

            while (smaller != 0){

               intermed = smaller;
               smaller = larger%smaller;
               larger = intermed;
            }
            comm = larger;
         }

         this.numerat = a/Math.abs(comm);
         this.denom = b/Math.abs(comm);
      }

   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.numerat;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.denom;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return Long.toString(this.numerat)+"/"+Long.toString(this.denom);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {

      if(((Lfraction) m).numerat == this.numerat && ((Lfraction) m).denom == this.denom)
      {
         return true;
      }else{

         return false;
      }

   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.numerat, this.denom); // TODO!!!
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      return new Lfraction((this.numerat*m.denom)+(m.numerat*this.denom),(this.denom*m.denom));
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {

      long denomnew = this.denom * m.denom;
      long numernew = this.numerat * m.numerat;
      return new Lfraction(numernew, denomnew);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if(this.numerat > 0) {
         return new Lfraction(this.denom, this.numerat); // TODO!!!
      }else if(this.numerat==0){
         throw new ArithmeticException("Can't inverse fraction beacause denominator can't be equal to 0");
      }else{
         return new Lfraction(-1*this.denom, -1*this.numerat);
      }
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-1*this.numerat, this.denom);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {

      if(this.denom == m.denom){
         return new Lfraction((this.numerat-m.numerat), this.denom);
      } else {
         return new Lfraction(((this.numerat*m.denom)- (m.numerat*this.denom)), (this.denom*m.denom));
      }
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {

      if (this.denom == 0 || m.denom == 0){
         throw new ArithmeticException("Denominator can't be 0");
      }else{
         return this.times(m.inverse());
      }

   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {

      if(this.numerat*m.denom<this.denom*m.numerat){
         return -1;
      } else if (this.numerat*m.denom>this.denom*m.numerat){
         return 1;
      } else {
         return 0;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerat, this.denom);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.numerat/this.denom; // TODO!!!
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return this.minus(new Lfraction(integerPart(), 1));
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {

      return (double)this.numerat/this.denom; // TODO!!!
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction((int) Math.round(f*d), d); // TODO!!!
      //new Answer((int) Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {

      long numerinpt;
      long denominpt;

      String[] newstr = s.split("/");
      if(newstr.length == 0){
         throw new RuntimeException("Invalid input "+s+" please check operator");
      }else {

         try {
            numerinpt = Long.parseLong(newstr[0]);
         } catch (Exception e) {
            throw new RuntimeException("Invalid input " + s + " please check numerator");
         }
         try {
            denominpt = Long.parseLong(newstr[1]);
         } catch (Exception e) {
            throw new RuntimeException("Invalid input " + s + " please check denominator");
         }


         return new Lfraction(numerinpt, denominpt); // TODO!!!
      }
   }
}

